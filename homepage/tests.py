from django.test import TestCase, Client
from django.urls import resolve
from homepage.views import index, story8, story9, afterlogin, logoutstory9
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class HomepageUnittest(TestCase):
    def test_show_url_is_exist(self):
        response= Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        
    def test_show_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
        
    def test_story8_url_is_exist(self):
        response= Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8.html')
        
    def test_story8_using_index_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8)
        
    def test_story9_url_is_exist(self):
        response= Client().get('/story9/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9.html')
        
    def test_story9_using_index_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, story9)
        
    def test_afterlogin_url_is_exist(self):
        response= Client().get('/story9/afterlogin')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'afterlogin.html')
        
    def test_afterlogin_using_index_func(self):
        found = resolve('/story9/afterlogin')
        self.assertEqual(found.func, afterlogin)
        
    def test_redirect_afterlogout(self):
        response = Client().get('/story9/logout')
        self.assertEqual(response.status_code, 302)
        
    
        
    
        
class StorysevenFunctionalTest(TestCase):
   
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StorysevenFunctionalTest, self).setUp()

    def tearDown(self):
        time.sleep(5)
        self.selenium.quit()
        super(StorysevenFunctionalTest, self).tearDown()
    
    def test_klik(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        buttton1 = selenium.find_element_by_id('act')
        buttton2 = selenium.find_element_by_id('org')
        buttton3 = selenium.find_element_by_id('pres')
        button4 = selenium.find_element_by_id('theme-button')

        buttton1.click()
        time.sleep(5)
        buttton2.click()
        time.sleep(5)
        buttton3.click()
        time.sleep(5)
        button4.click()
        time.sleep(5)

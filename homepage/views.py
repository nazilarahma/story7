from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.
def index(request):
    return render(request, 'index.html')
    
def story8(request):
    return render(request, 'story8.html')
    
def story9(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('homepage:afterlogin')
    else:
        form = AuthenticationForm()
    return render(request, 'story9.html', {'form':form})
        
def afterlogin(request):
    return render(request, 'afterlogin.html')
    
def logoutstory9(request):
	if request.user.is_authenticated:
		if request.method == 'POST':
			logout(request)
	return redirect('homepage:story9')
    
    
    

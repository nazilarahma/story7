from django.contrib import admin
from . import views
from django.urls import path

app_name='homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('story8/', views.story8, name='story8'),
    path('story9/', views.story9, name='story9'),
    path('story9/afterlogin', views.afterlogin, name='afterlogin'),
    path('story9/logout', views.logoutstory9, name='logoutstory9'),
    # path('', views.redirecting, name='redirecting')
]
